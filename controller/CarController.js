const {cars} = require('../models');

module.exports = class {
    static addCar(req, res) {
        cars.create({
            nama: req.body.nama,
            tipe: req.body.tipe,
            biaya: req.body.biaya,
            ukuran: req.body.ukuran,
            foto: req.body.foto
        }).then((result) => {
            res.status(201).json({
                status: 201,
                message: "Berhasil Menambahkan Data",
                data: result
            })
        }).catch((err) => {
            console.log(err);
        });
    }

    static getCar(req, res) {
        cars.findAll().then((result) => {
            res.status(200).json({
                status: 200,
                message: "Berhasil Mengambil Data",
                data: result
            })
        }).catch((err) => {
            console.log(err);
        });
    }

    static updateCar(req, res) {
        cars.update(req.body, {
            where: {
                id: req.params.id
            }
        }).then((result) => {
            res.status(201).json({
                status: 201,
                message: "Berhasil Mengupdate Data",
                data: req.body
            })
        }).catch((err) => {
            console.log(err)
        });
    }

    static deleteCar(req, res) {
        cars.destroy({
            where: {
                id: req.params.id
            }
        }).then((result) => {
            res.status(200).json({
                status: 200,
                message: "Berhasil Menghapus Data"
            })
        }).catch((err) => {
            console.log(err);
        });
    }
}