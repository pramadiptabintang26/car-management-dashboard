var express = require('express');
var router = express.Router();
const CarController = require('../controller/CarController')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/cars/register', CarController.addCar);
router.get('/cars', CarController.getCar);
router.put('/cars/:id', CarController.updateCar);
router.delete('/cars/:id', CarController.deleteCar);

module.exports = router;